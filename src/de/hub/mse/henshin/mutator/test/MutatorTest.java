package de.hub.mse.henshin.mutator.test;

import java.io.IOException;
import java.util.Collections;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.eclipse.emf.henshin.trace.TracePackage;
import org.eclipse.uml2.uml.UMLPackage;

import GraphConstraint.GraphConstraintPackage;
import de.hub.mse.henshin.mutator.edit.Mutator;
import de.imotep.featuremodel.variability.metamodel.FeatureModel.FeatureModelPackage;
import pivot.PivotPackage;
import symmetric.SymmetricPackage;

public class MutatorTest {

	static {
		// Initialize the meta-models
		EcorePackage.eINSTANCE.eClass();
		FeatureModelPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();
		SymmetricPackage.eINSTANCE.eClass();
		GraphConstraintPackage.eINSTANCE.eClass();
		PivotPackage.eINSTANCE.eClass();
		TracePackage.eINSTANCE.eClass();
	}
	
	public static void main(String[] args) throws IOException {
		HenshinResourceSet resSet = new HenshinResourceSet("test");
		Module module = resSet.getModule("Original.henshin");
		Rule rule = (Rule) module.getUnits().get(0);
		
		System.out.println("= Mutate rule: " + rule.getName());
		Mutator mutator = new Mutator(rule, 5, false);
		mutator.mutate();
		
		System.out.println("Save...");
		Resource mutateResource = resSet.createResource("Mutated.henshin");
		mutateResource.getContents().add(module);
		mutateResource.save(Collections.EMPTY_MAP);
	}
}
