package de.hub.mse.henshin.mutator.edit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.henshin.model.Action;
import org.eclipse.emf.henshin.model.Action.Type;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import de.hub.mse.henshin.mutator.edit.BasicEditor.ContextType;
import de.hub.mse.henshin.mutator.edit.BasicEditor.EditOperationType;

public class RandomSelector {

	private Rule rule;

	private List<BasicEditor.ContextType> contextTypes = new ArrayList<BasicEditor.ContextType>();
	private List<BasicEditor.EditOperationType> editOperationTypes = new ArrayList<BasicEditor.EditOperationType>();
	private List<Action.Type> actionTypes = new ArrayList<Action.Type>();

	private List<Node> allActionNodes = new ArrayList<Node>();
	private Map<EClass, List<Node>> actionNodesByType = new HashMap<EClass, List<Node>>();
	private List<Edge> allActionEdges = new ArrayList<Edge>();

	private List<EClass> allNodeTypes = new ArrayList<EClass>();
	private List<EReference> allEdgeTypes = new ArrayList<EReference>();

	public RandomSelector(Rule rule) {
		this.rule = rule;

		// static stuff
		initOperationInfo();

		// Get types from metamodel(s) over which rule is typed
		initTypeInfo();

		// index the rule graph elements
		initOrUpdateRuleInfo();
	}

	private void initOperationInfo() {
		contextTypes.add(BasicEditor.ContextType.NODE);
		contextTypes.add(BasicEditor.ContextType.EDGE);

		editOperationTypes.add(BasicEditor.EditOperationType.INSERT);
		editOperationTypes.add(BasicEditor.EditOperationType.REMOVE);
		editOperationTypes.add(BasicEditor.EditOperationType.UPDATE);

		actionTypes.add(Action.Type.PRESERVE);
		actionTypes.add(Action.Type.CREATE);
		actionTypes.add(Action.Type.DELETE);
//		actionTypes.add(Action.Type.FORBID);
//		actionTypes.add(Action.Type.REQUIRE);
	}

	private void initTypeInfo() {
		for (EPackage p : rule.getModule().getImports()) {
			for (Iterator<EObject> iterator = p.eAllContents(); iterator.hasNext();) {
				EObject obj = iterator.next();
				if (obj instanceof EClass) {
					EClass nodeType = (EClass) obj;
					allNodeTypes.add(nodeType);
				}
				if (obj instanceof EReference) {
					EReference edgeType = (EReference) obj;
					allEdgeTypes.add(edgeType);
				}
			}
		}
	}

	private void initOrUpdateRuleInfo() {
		allActionNodes = new ArrayList<Node>();
		actionNodesByType = new HashMap<EClass, List<Node>>();
		allActionEdges = new ArrayList<Edge>();

		for (Iterator<EObject> iterator = rule.eAllContents(); iterator.hasNext();) {
			EObject obj = iterator.next();
			if (obj instanceof Node) {
				Node node = (Node) obj;
				if (node.getAction() != null) {
					allActionNodes.add(node);

					if (!actionNodesByType.containsKey(node.getType())) {
						actionNodesByType.put(node.getType(), new ArrayList<Node>());
					}
					actionNodesByType.get(node.getType()).add(node);
				}
			}
			if (obj instanceof Edge) {
				Edge edge = (Edge) obj;
				if (edge.getAction() != null) {
					allActionEdges.add(edge);
				}
			}
		}
	}

	public BasicEditor.ContextType selectContextType() {
		return (ContextType) getRandomlyFromList(contextTypes);
	}

	public BasicEditor.EditOperationType selectEditOperationType() {
		return (EditOperationType) getRandomlyFromList(editOperationTypes);
	}

	public Action.Type selectActionType(List<Action.Type> blackList) {
		while (true) {
			Action.Type type = (Type) getRandomlyFromList(actionTypes);
			if (!blackList.contains(type)) {
				return type;
			}
		}
	}

	public EClass selectNodeType(boolean excludeAbstract) {
		while (true) {
			EClass type = (EClass) getRandomlyFromList(allNodeTypes);
			if (excludeAbstract && type.isAbstract()) {
				continue;
			} else {
				return type;
			}
		}
	}

	public EReference selectEdgeType(boolean excludeDerived) {
		while (true) {
			EReference type = (EReference) getRandomlyFromList(allEdgeTypes);
			if (excludeDerived && type.isDerived()) {
				continue;
			} else {
				return type;
			}
		}
	}

	public Node selectNode() {
		return (Node) getRandomlyFromList(allActionNodes);
	}

	public Node selectNode(EClass nodeType) {
		return (Node) getRandomlyFromList(actionNodesByType.get(nodeType));
	}

	public Edge selectEdge() {
		return (Edge) getRandomlyFromList(allActionEdges);
	}

	public void ruleChanged() {
		initOrUpdateRuleInfo();
	}

	private Object getRandomlyFromList(List list) {
		if (list == null || list.isEmpty()) {
			return null;
		}

		int idx = (int) ((Math.random()) * list.size());
		return list.get(idx);
	}
}
