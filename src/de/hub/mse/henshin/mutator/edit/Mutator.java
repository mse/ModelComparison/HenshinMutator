package de.hub.mse.henshin.mutator.edit;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

public class Mutator {

	private int numberOfEditOperations;

	private RandomSelector selector;
	private BasicEditor editor;
	private boolean quiet;

	public Mutator(Rule rule, int numberOfEditOperations, boolean quiet) {
		// Throws a RuntimeException if metamodel is not available
		checkMetamodelAvailability(rule);

		this.numberOfEditOperations = numberOfEditOperations;

		this.selector = new RandomSelector(rule);
		this.editor = new BasicEditor(rule, selector);
		this.quiet = quiet;
	}

	public void mutate() {
		int counter = 0;

		while (counter < numberOfEditOperations) {
			BasicEditor.ContextType contextType = selector.selectContextType();
			BasicEditor.EditOperationType operationType = selector.selectEditOperationType();

			boolean successful = false;

			// Call proper edit operation
			if (contextType == BasicEditor.ContextType.NODE) {
				if (operationType == BasicEditor.EditOperationType.INSERT) {
					successful = editor.insertNode(quiet);
				}
				if (operationType == BasicEditor.EditOperationType.UPDATE) {
					successful = editor.updateNode(quiet);
				}
				if (operationType == BasicEditor.EditOperationType.REMOVE) {
					successful = editor.removeNode(quiet);
				}
			}
			if (contextType == BasicEditor.ContextType.EDGE) {
				if (operationType == BasicEditor.EditOperationType.INSERT) {
					successful = editor.insertEdge(quiet);
				}
				if (operationType == BasicEditor.EditOperationType.UPDATE) {
					successful = editor.updateEdge(quiet);
				}
				if (operationType == BasicEditor.EditOperationType.REMOVE) {
					successful = editor.removeEdge(quiet);
				}
			}

			if (successful) {
				counter++;
			}
		}
	}

	private void checkMetamodelAvailability(Rule rule) {
		for (Iterator<EObject> iterator = rule.eAllContents(); iterator.hasNext();) {
			EObject obj = iterator.next();
			if (obj instanceof Node) {
				Node node = (Node) obj;
				if (node.getType().eIsProxy()) {
					String proxy = node.getType().toString();
					String proxyUri = proxy.substring(proxy.indexOf("eProxyURI: ") + 11, proxy.length() - 1);

					throw new RuntimeException("At least one metamodel is unknown, cannot resolve type: " + proxyUri);
				}
			}
		}
	}

}
