package de.hub.mse.henshin.mutator.edit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.henshin.model.Action;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.util.HenshinModelCleaner;

public class BasicEditor {

	public static enum EditOperationType {
		INSERT, REMOVE, UPDATE;
	}

	public static enum ContextType {
		NODE, EDGE;
	}

	private Rule rule;
	private RandomSelector selector;

	public BasicEditor(Rule rule, RandomSelector selector) {
		this.rule = rule;
		this.selector = selector;
	}
	
	public Rule getRule() {
		return rule;
	}

	public boolean insertNode(boolean quiet) {
		// Randomly select action and type:
		EClass type = selector.selectNodeType(true);
		Action action = new Action(selector.selectActionType(Collections.EMPTY_LIST));

		if (!quiet)
			System.out.println("Mutate: INSERT_NODE " + type.getName() + " " + action.getType());

		// Create node:
		Node node = rule.createNode(type);

		// Update containment:
		updateContainment(rule, node);

		// Set action:
		node.setAction(action);

		// Complete multi-rules:
		HenshinModelCleaner.completeMultiRules(rule.getRootRule());

		// Clean up:
		HenshinModelCleaner.cleanRule(rule.getRootRule());

		selector.ruleChanged();

		return true;
	}

	public boolean updateNode(boolean quiet) {
		// Prevent empty rule graphs
		if (rule.getLhs().getNodes().size() <= 1 || rule.getRhs().getNodes().size() <= 1) {
			return false;
		}
		
		// Randomly select node and new action
		Node node = selector.selectNode();
		if (node == null) {
			return false;
		}
		Action action = new Action(selector.selectActionType(getBlackList(node.getAction().getType())));

		if (!quiet)
			System.out.println("Mutate: UPDATE_NODE " + node + " " + action.getType());

		// Set action:
		node.setAction(action);

		// Complete multi-rules:
		HenshinModelCleaner.completeMultiRules(rule.getRootRule());

		// Clean up:
		HenshinModelCleaner.cleanRule(rule.getRootRule());

		selector.ruleChanged();

		return true;
	}

	public boolean removeNode(boolean quiet) {
		// Prevent empty rule graphs
		if (rule.getLhs().getNodes().size() <= 1 || rule.getRhs().getNodes().size() <= 1) {
			return false;
		}
		
		// Randomly select node
		Node node = selector.selectNode();
		if (node == null) {
			return false;
		}

		if (!quiet)
			System.out.println("Mutate: REMOVE_NODE " + node);

		// Remove the node:
		rule.removeNode(node, true);

		// Clean up:
		HenshinModelCleaner.cleanRule(rule.getRootRule());

		selector.ruleChanged();

		return true;
	}

	public boolean insertEdge(boolean quiet) {
		// Randomly select type, source and target:
		EReference type = selector.selectEdgeType(true);
		Node source = selector.selectNode(type.getEContainingClass());
		Node target = selector.selectNode(type.getEReferenceType());

		if (source == null || target == null || !rule.canCreateEdge(source, target, type)) {
			return false;
		}

		if (!quiet)
			System.out.println("Mutate: INSERT_EDGE " + type.getName() + " " + source + " " + target);
		
		// Create the edge:
		Edge edge = rule.createEdge(source, target, type);

		// Complete multi-rules:
		HenshinModelCleaner.completeMultiRules(rule.getRootRule());

		// Clean up:
		HenshinModelCleaner.cleanRule(rule.getRootRule());

		selector.ruleChanged();

		return true;
	}

	public boolean updateEdge(boolean quiet) {
		// Randomly select edge and new action
		Edge edge = selector.selectEdge();
		if (edge == null) {
			return false;
		}
		Action action = new Action(selector.selectActionType(getBlackList(edge.getAction().getType())));

		if (!quiet)
			System.out.println("Mutate: UPDATE_EDGE " + edge + " " + action.getType());

		// Set action:
		edge.setAction(action);

		// Complete multi-rules:
		HenshinModelCleaner.completeMultiRules(rule.getRootRule());

		// Clean up:
		HenshinModelCleaner.cleanRule(rule.getRootRule());

		selector.ruleChanged();

		return true;
	}

	public boolean removeEdge(boolean quiet) {
		// Randomly select edge
		Edge edge = selector.selectEdge();
		if (edge == null) {
			return false;
		}
		if (!quiet)
			System.out.println("Mutate: REMOVE_EDGE " + edge);

		// Remove the node:
		rule.removeEdge(edge, true);

		// Clean up:
		HenshinModelCleaner.cleanRule(rule.getRootRule());

		selector.ruleChanged();

		return true;
	}

	/*
	 * Update containment for the new node.
	 */
	private void updateContainment(Rule rule, Node newNode) {
		// Check if it makes sense to create a containment edge to the new node:
		if (!newNode.getIncoming().isEmpty()) {
			return;
		}
		for (Node container : rule.getActionNodes(null)) {
			if (container == newNode || container.getType() == null) {
				continue;
			}
			for (EReference ref : container.getType().getEAllContainments()) {
				EClass refType = ref.getEReferenceType();
				if (refType != null && (refType.isSuperTypeOf(newNode.getType())) || (newNode.getType()).isSuperTypeOf(refType)) {
					if (rule.canCreateEdge(container, newNode, ref)) {
						rule.createEdge(container, newNode, ref);
						return;
					}
				}
			}
		}
	}

	private List<Action.Type> getBlackList(Action.Type type) {
		ArrayList<Action.Type> blackList = new ArrayList<Action.Type>();
		blackList.add(type);

		return blackList;
	}
}
